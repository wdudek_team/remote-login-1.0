from user import User
import sqlite3

class UserRepository:
    def __connect(self):
        self.db = sqlite3.connect('data/login.mydb')
        return self.db

    def __close(self):
        self.db.close()

    def init(self):
        with self.__connect() as db:
            cursor = db.cursor()
            cursor.execute('''
                SELECT * FROM sqlite_master WHERE type='table' AND name='users'
            ''')
            result = cursor.fetchone()
            if not result:
                print("Adding users table")
                cursor.execute('''
                    CREATE TABLE users(id INTEGER PRIMARY KEY, first_name TEXT, surname TEXT,
                                   login TEXT, password TEXT,
                                   phone TEXT, email TEXT unique)
                ''')
                db.commit()

    def create(self,user):
        assert isinstance(user,User)
        with self.__connect() as db:
            cursor = db.cursor()
            print("Adding user: %s"%user)
            print(cursor.execute('''INSERT OR REPLACE INTO users(first_name, surname, login, password, phone, email)
                  VALUES(:_firstName,:_surname,:_login, :_password, :_phone, :_email)''', vars(user)))
            db.commit()