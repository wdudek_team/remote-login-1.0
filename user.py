import getpass
from Crypto.Cipher import AES
import base64

#TODO make secure
secret_key = "1234567890"

class User:

    def __init__(self, firstName=None, surname=None, email=None, login=None, password=None, phone=None):
        self._firstName = firstName
        self._lastName = surname
        self._email = email
        self._login = login
        self._password = password
        self._phone = phone

    @property
    def firstName(self):
        return self._firstName

    @property
    def surname(self):
        return self._surname

    @property
    def email(self):
        return self._email

    @property
    def login(self):
        return self._login

    @property
    def password(self):
        return self._password

    @property
    def phone(self):
        return self._phone

    @firstName.setter
    def firstName(self, value):
        self._firstName = value

    @surname.setter
    def surname(self, value):
        self._surname = value

    @email.setter
    def email(self, value):
        self._email = value

    @login.setter
    def login(self, value):
        self._slogin = value

    @password.setter
    def password(self, value):
        self._password = value

    @phone.setter
    def phone(self, value):
        self._phone = value

    def askFirstName(self):
        name=None
        while not name:
            name=input("Get first name:")
        self._firstName=name

    def askSurname(self):
        name=None
        while not name:
            name=input("Get surname:")
        self._firstName=name

    def askPassword(self):
        password = None
        while not password:
            password = getpass.getpass('Password:')
        #secret_key = getpass.getpass('Get secret key:')
        cipher = AES.new(secret_key.rjust(16), AES.MODE_ECB)
        self._password = base64.b64encode(cipher.encrypt(password.rjust(16)))

    def askAll(self):
        self.askPassword()

    def readPassword(self):
        #secret_key = getpass.getpass('Get secret key:')
        cipher = AES.new(secret_key.rjust(16), AES.MODE_ECB)
        return cipher.decrypt(base64.b64decode(self._password)).strip()


