import requests

urlLogon='https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogOn'
urlLogin = 'https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogIn'
urlPortal='https://portalpacjenta.luxmed.pl/PatientPortal/'

headers={'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
'Accept-Encoding': 'gzip, deflate, br',
'Accept-Language': 'pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7',
'Connection': 'keep-alive',
'Content-Length': '33',
'Content-Type': 'application/x-www-form-urlencoded',
'Host': 'portalpacjenta.luxmed.pl',
'Origin': 'https://portalpacjenta.luxmed.pl',
'Referer': 'https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogOn',
'Upgrade-Insecure-Requests': '1',
'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'}

payload={'Login': 'user','Password':'secretpass'}

s = requests.Session()
resp = s.get(url=urlLogon)
print(resp.status_code)
print(resp.content)
resp = s.post(url=urlLogin,data=payload, headers=headers)
print(resp.status_code)
print(resp.content)
resp = s.get(url=urlPortal)
print(resp.status_code)
print(resp.content)
